import psycopg2
import string        
import random

def random_Generator_text(size=10):
    return ''.join(random.choice(string.ascii_uppercase)for x in range(size))

def random_Generator_number(min_=1, max_=100000):
    return random.randint(min_, max_)

def add_data_to_database():
    try:
        connect_to_database = psycopg2.connect(database="demo_db", user="postgres", password="root", host="127.0.0.1", port="5432")
        console = connect_to_database.cursor()
    except:
        print ("I am unable to connect to the database")
        
    F_Name = random_Generator_text(20)
    T_Name = random_Generator_text(20)
        
    values = [F_Name, random_Generator_number(50000,100000)]
    try:
        console.execute("INSERT INTO Football_field (F_Name, Capacity) VALUES (%s,%s)",tuple(values))
        connect_to_database.commit()
    except:
        print('Except add Football_field !!')

    values = [T_Name, random_Generator_text(20), F_Name]
    try:
        console.execute("INSERT INTO Team (T_Name, Name_manager, Field_name) VALUES (%s,%s,%s)",tuple(values))
        connect_to_database.commit()
    except:
        print('Except add Team !!')

    count_player = 0
    random_count = random.choice([20, 25, 30, 40])
    while (count_player < random_count):
        try:
            player_number = random_Generator_number(1,99)
            ID = random_Generator_number(50000,900000)
            values = [ID, player_number, random_Generator_text(20), random_Generator_text(20), random.choice(['L', 'R','ALL']), T_Name]
            console.execute("INSERT INTO Player (ID_Player, P_Number, P_Name, P_Last, Skill, TName) VALUES (%s,%s,%s,%s,%s,%s)",tuple(values))
            connect_to_database.commit()
            values = [ID, random.choice(['En', 'Th','AF', 'AS', 'KE'])]
            console.execute("INSERT INTO Nationality (ID_Player, Nationality) VALUES (%s,%s)",tuple(values))
            connect_to_database.commit()
            count_player = count_player + 1
            print('Add Data Ok...  :)')
        except:
            print('Except add Player !!')
            break

def main():
    count = 0
    while(count < 1000):
        add_data_to_database()
        count = count + 1
        print('Add Data To Database At : ',count)
        

if __name__ == "__main__":
    main()

